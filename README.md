Pharmacy recommender system
==============================

This is a recommender system for drugs based on the user's symptoms. Dataset and model is private, so it's only for demonstration purposes.
Project consists of two parts:
- Backend - FastAPI application with a recommender system serving.
- Pipeline - data processing and training pipeline for the recommender system. Created with Luigi, MLFlow, PyTorch.

## Tech Stack

**Backend:** FastAPI, Pydantic, Gunicorn, Uvicorn

**Pipeline** Luigi, MLFlow, PyTorch, Pandas

## Run Locally

Copy example.env to .env and fill in the variables

```bash
  cp example.env .env
```

Install dependencies

```bash
  make requirements
```

To run the serving backend run the uvicorn server

```bash
  make start_app_local
```

To run the pipeline locally

```bash
  make start_pipeline_local
```

