import os

import mlflow

from pipeline.train.utils.model_wrapper import DrugPredictionWrapper
from pipeline.config import BEST_MODELS_PATH


def get_drug_prediction_pipeline() -> DrugPredictionWrapper:
    model_path = os.path.abspath(BEST_MODELS_PATH)
    return mlflow.pyfunc.load_model(model_path).unwrap_python_model()
