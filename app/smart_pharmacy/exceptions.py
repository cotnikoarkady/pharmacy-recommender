from fastapi import HTTPException


class InvalidICD10Code(HTTPException):
    def __init__(self, detail: str = "Invalid ICD10CM code"):
        super().__init__(status_code=422, detail=detail)
