from fastapi import APIRouter, Body, Query, Depends

from pipeline.train.utils.model_wrapper import DrugPredictionWrapper
from .dependencies import get_drug_prediction_pipeline
from .schemas import Patient

router = APIRouter(tags=["smart_pharmacy"], prefix="/smart_pharmacy")


@router.post("/recommend_drug")
async def recommend_drug(
    patient: Patient = Body(..., description="Patient information"),
    number_of_predictions: int = Query(default=5, example=5, ge=1, description="Number of drugs to recommend"),
    drug_prediction_pipeline: DrugPredictionWrapper = Depends(
            get_drug_prediction_pipeline
    )
) -> dict:
    probabilities = drug_prediction_pipeline.predict(
        model_input=patient.conditions, context=None, top_k=number_of_predictions
    )
    return probabilities
