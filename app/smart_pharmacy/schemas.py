from pydantic import BaseModel, Field, field_validator

from .exceptions import InvalidICD10Code


class Patient(BaseModel):
    conditions_icd10cm: list[str] = Field(
        ..., description="List of ICD10CM codes for the patient's conditions", example=["F4310", "R45851", "F314"]
    )
    age: int = Field(..., ge=18, description="Age of the patient in years", example=25)

    @field_validator("conditions_icd10cm")
    def check_conditions_icd10cm(cls, v):
        if len(v) == 0:
            raise ValueError("conditions cannot be empty")
        for condition in v:
            if condition == "":
                raise InvalidICD10Code("condition cannot be empty")
            if len(condition) > 7:
                raise InvalidICD10Code("condition cannot be longer than 7 characters")
            if not condition[0].isalpha():
                raise InvalidICD10Code("condition must start with a letter")
            if not condition[1:].isdigit():
                raise InvalidICD10Code("condition must end with a number")
        return v
