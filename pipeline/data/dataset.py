import os

import luigi
import pandas as pd
from tqdm import tqdm

from pipeline.config import DEFAULT_DATASET_DIR
from pipeline.data.func.filters import is_mental_disease, is_psychiatric_drug
from pipeline.data.func.mappings import convert_ndc2atc, icd9mapping
from pipeline.data.raw_dataset import RawAdmissions, RawDiagnoses, RawPatients, RawPrescriptions


class Dataset(luigi.Task):
    def requires(self) -> dict[str, luigi.Task]:
        return {
            "admissions": RawAdmissions(),
            "patients": RawPatients(),
            "diagnoses": RawDiagnoses(),
            "prescriptions": RawPrescriptions(),
        }

    def run(self) -> None:
        os.makedirs(self.output().path, exist_ok=True)
        patients_ids, visits_ids = self.find_ids_with_mental_diseases()
        for table_name, table in tqdm(self.input().items()):
            table_df = open_table(table, table_name)
            table_df = filter_mental_entities(patients_ids, table_df, visits_ids)
            table_df = map_codes(table_df)
            table_df.to_csv(os.path.join(self.output().path, f"{table_name}.csv"), index=False)

    def find_ids_with_mental_diseases(self):
        diagnoses = pd.read_csv(self.input()["diagnoses"].path, dtype={"subject_id": str, "hadm_id": str})
        main_diagnoses = diagnoses[diagnoses["seq_num"] == 1]
        main_diagnoses["icd_code"] = main_diagnoses["icd_code"].map(icd9mapping)
        main_diagnoses.dropna(inplace=True)
        mental_diseases = main_diagnoses[main_diagnoses["icd_code"].apply(is_mental_disease)]
        patients_ids = mental_diseases["subject_id"].unique()
        visits_ids = mental_diseases["hadm_id"].unique()
        return patients_ids, visits_ids

    def output(self) -> dict[str, luigi.LocalTarget]:
        output_path = DEFAULT_DATASET_DIR
        return {
            "admissions": luigi.LocalTarget(os.path.join(output_path, "admissions.csv")),
            "patients": luigi.LocalTarget(os.path.join(output_path, "patients.csv")),
            "diagnoses": luigi.LocalTarget(os.path.join(output_path, "diagnoses.csv")),
            "prescriptions": luigi.LocalTarget(os.path.join(output_path, "prescriptions.csv")),
        }


def open_table(table, table_name):
    if table_name == "admissions":
        return pd.read_csv(table.path, dtype={"subject_id": str, "hadm_id": str})
    elif table_name == "patients":
        return pd.read_csv(table.path, dtype={"subject_id": str})
    elif table_name == "diagnoses":
        return pd.read_csv(table.path, dtype={"subject_id": str, "hadm_id": str})
    elif table_name == "prescriptions":
        return pd.read_csv(table.path, dtype={"subject_id": str, "hadm_id": str, "ndc": str})
    else:
        raise ValueError(f"Unknown table name {table_name}")


def filter_mental_entities(patients_ids, table_df, visits_ids):
    table_df = table_df[table_df["subject_id"].isin(patients_ids)]
    if "subject_id" in table_df.columns:
        table_df = table_df[table_df["subject_id"].isin(patients_ids)]
    if "hadm_id" in table_df.columns:
        table_df = table_df[table_df["hadm_id"].isin(visits_ids)]
    return table_df


def map_codes(table_df):
    # ndc -> atc (prescriptions)
    if "ndc" in table_df.columns:
        table_df["atc"] = table_df["ndc"].apply(convert_ndc2atc)
        table_df = table_df[table_df["atc"].apply(is_psychiatric_drug)]
    # icd9 -> icd10 (diagnoses)
    if "icd_code" in table_df.columns:
        table_df["icd_code"] = table_df["icd_code"].map(icd9mapping)
        table_df.dropna(inplace=True)
    return table_df
