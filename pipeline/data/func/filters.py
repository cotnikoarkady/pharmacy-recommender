def is_psychiatric_drug(atc_code):
    if not atc_code:
        return False
    atc_code = atc_code[0]
    psycholeptics = "N05"
    psychoanaleptics = "N06"
    monoamine_oxidase_B_inhibitors = "N04BD"
    folic_acid = "B03BB01"
    gabapentinoids = "N02BF"
    propranol = "C07AA05"
    return (
        atc_code in (folic_acid, gabapentinoids, propranol)
        or atc_code[:3] == psycholeptics
        or atc_code[:3] == psychoanaleptics
        or atc_code[:5] == monoamine_oxidase_B_inhibitors
    )


def is_mental_disease(icd_code):
    first_letter = icd_code[0]
    is_icd9 = first_letter == "V" or first_letter.isdigit()
    if is_icd9:
        return _is_mental_disorder_icd9(icd_code)
    else:
        return _is_mental_disorder_icd10(icd_code)


def _is_mental_disorder_icd9(code):
    code = str(code)
    if code[0] == "V":
        return False
    code_class = int(code[:3])
    alcohol_dependence = 303
    abuse_of_drugs = 305
    opioid_abuse = 304
    return (
        290 <= code_class <= 319
        and code_class != alcohol_dependence
        and code != abuse_of_drugs
        and code != opioid_abuse
    )


def _is_mental_disorder_icd10(code):
    # exclude non-mental disorders
    dependence_syndromes_icd10 = ["F10", "F19", "F14", "F11"]
    return (code[0] == "F") and code[:3] not in dependence_syndromes_icd10
