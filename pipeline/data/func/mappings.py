import pandas as pd
from pyhealth.medcode import CrossMap

from pipeline.config import ICD9_TO_ICD10_MAPPING_PATH

ndc2atc = CrossMap("NDC", "ATC")


def convert_ndc2atc(ndc):
    return ndc2atc.map(ndc)


def create_icd9_to_icd10_mapping(mapping_path):
    icd9_to_icd10_df = pd.read_table(mapping_path)
    icd9mapping = {}
    icd9_to_icd10_df = icd9_to_icd10_df[icd9_to_icd10_df["icd10cm"] != "NoDx"]
    for _index, row in icd9_to_icd10_df.iterrows():
        icd9mapping[row["icd9cm"]] = row["icd10cm"]
        icd9mapping[row["icd10cm"]] = row["icd10cm"]
    return icd9mapping


icd9mapping = create_icd9_to_icd10_mapping(ICD9_TO_ICD10_MAPPING_PATH)
