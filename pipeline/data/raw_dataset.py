import os

import luigi

from pipeline.config import DEFAULT_RAW_DATA_DIR


class RawAdmissions(luigi.ExternalTask):
    raw_data_dir: str = luigi.Parameter(default=DEFAULT_RAW_DATA_DIR)

    def output(self) -> luigi.LocalTarget:
        return luigi.LocalTarget(os.path.join(self.raw_data_dir, "admissions.csv.gz"))


class RawPatients(luigi.ExternalTask):
    raw_data_dir: str = luigi.Parameter(default=DEFAULT_RAW_DATA_DIR)

    def output(self) -> luigi.LocalTarget:
        return luigi.LocalTarget(os.path.join(self.raw_data_dir, "patients.csv.gz"))


class RawDiagnoses(luigi.ExternalTask):
    raw_data_dir: str = luigi.Parameter(default=DEFAULT_RAW_DATA_DIR)

    def output(self) -> luigi.LocalTarget:
        return luigi.LocalTarget(os.path.join(self.raw_data_dir, "diagnoses_icd.csv.gz"))


class RawPrescriptions(luigi.ExternalTask):
    raw_data_dir: str = luigi.Parameter(default=DEFAULT_RAW_DATA_DIR)

    def output(self) -> luigi.LocalTarget:
        return luigi.LocalTarget(os.path.join(self.raw_data_dir, "prescriptions.csv.gz"))
