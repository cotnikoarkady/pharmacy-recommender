import os
import pickle

import luigi
import pandas as pd
from tqdm import tqdm

from pipeline.config import DEFAULT_FEATURES_DIR
from pipeline.data.dataset import Dataset


class Samples(luigi.Task):
    """
    This task creates a dataset of samples from the processed dataset.
    Samples have structure like this:
    {
        "visit_id": visit.visit_id,
        "patient_id": patient.patient_id,
        "conditions": conditions,
        "drugs": drugs,
        "age": age,
        "gender": gender
    }
    """

    def requires(self):
        return Dataset()

    def run(self):
        os.makedirs(DEFAULT_FEATURES_DIR, exist_ok=True)
        admissions = pd.read_csv(self.input()["admissions"].path)
        diagnoses = pd.read_csv(self.input()["diagnoses"].path)
        prescriptions = pd.read_csv(self.input()["prescriptions"].path)
        patient = pd.read_csv(self.input()["patients"].path)

        samples = []
        for _, admission in tqdm(admissions.iterrows()):
            patient_id = admission["subject_id"]
            visit_id = admission["hadm_id"]
            conditions = diagnoses[diagnoses["hadm_id"] == visit_id]["icd_code"].tolist()
            drugs = prescriptions[prescriptions["hadm_id"] == visit_id]["atc"].tolist()
            anchor_age = patient[patient["subject_id"] == patient_id]["anchor_age"].tolist()[0]
            gender = patient[patient["subject_id"] == patient_id]["gender"].tolist()[0]

            for i, drug in enumerate(drugs):
                drugs[i] = drug[2:-2]
            if len(drugs) == 0 or len(conditions) == 0:
                continue
            if anchor_age < 18:
                continue
            samples.append(
                {
                    "visit_id": visit_id,
                    "patient_id": patient_id,
                    "conditions": conditions,
                    "drugs": drugs,
                    "age": anchor_age,
                    "gender": gender,
                }
            )
        with open(self.output().path, "wb") as f:
            pickle.dump(samples, f)

    def output(self):
        return luigi.LocalTarget(os.path.join(DEFAULT_FEATURES_DIR, "samples.pkl"))
