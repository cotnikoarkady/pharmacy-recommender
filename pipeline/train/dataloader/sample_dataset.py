from torch.utils.data import Dataset, random_split


class SampleDataset(Dataset):
    def __init__(self, samples):
        self.samples = samples
        self.drugs_tokens = list(self.get_drugs_tokens())
        self.conditions_tokens = list(self.get_conditions_tokens())

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        return self.samples[idx]

    def get_conditions_tokens(self):
        tokens = set()
        for sample in self.samples:
            tokens.update(sample["conditions"])
        return tokens

    def get_drugs_tokens(self):
        tokens = set()
        for sample in self.samples:
            tokens.update(sample["drugs"])
        return tokens

    def split(self, train_size=0.8, val_size=0.1, test_size=0.1):
        """
        Splits the dataset into train, validation and test sets.
        """
        assert train_size + val_size + test_size == 1
        proportions = [train_size, val_size, test_size]
        lengths = [int(p * len(self)) for p in proportions]
        lengths[-1] = len(self) - sum(lengths[:-1])
        tr_dataset, vl_dataset, ts_dataset = random_split(self, lengths)
        return tr_dataset, vl_dataset, ts_dataset
