class Tokenizer:
    def __init__(self, tokens: list[str], special_tokens: list[str] = None):
        """Initializes the tokenizer.

        Args:
            tokens: List[str], list of tokens in the vocabulary.
            special_tokens: List[str], list of special tokens to add to the
                vocabulary. (e.g., <pad>, <unk>). Default is empty list.
        """
        if special_tokens is None:
            special_tokens = ["<pad>", "<unk>"]
        all_tokens = special_tokens + tokens
        self.token2idx = {}
        self.idx2token = {}
        self.idx = 0
        for token in all_tokens:
            self.add_token(token)

    def add_token(self, token):
        """Adds a token to the vocabulary."""
        if token not in self.token2idx:
            self.token2idx[token] = self.idx
            self.idx2token[self.idx] = token
            self.idx += 1

    def __call__(self, token):
        """Retrieves the index of the token.

        Note that if the token is not in the vocabulary, this function will try to
        return the index of <unk>. If <unk> is not in the vocabulary,
        an exception will be raised.
        """
        if token not in self.token2idx:
            if "<unk>" in self.token2idx:
                return self.token2idx["<unk>"]
            else:
                raise ValueError(f"Unknown token: {token}")
        return self.token2idx[token]

    def __len__(self):
        """Returns the size of the vocabulary."""
        return len(self.token2idx)

    def __contains__(self, token):
        return token in self.token2idx

    def convert_tokens_to_indices(self, tokens: list[str]) -> list[int]:
        """Converts a list of tokens to indices."""
        return [self.token2idx[token] for token in tokens]

    def convert_indices_to_tokens(self, indices: list[int]) -> list[str]:
        """Converts a list of indices to tokens."""
        return [self.idx2token[idx] for idx in indices]

    def batch_encode_2d(
        self, tokens_2d: list[list[str]], max_seq_length: int
    ) -> list[list[int]]:
        """Converts a 2d list of tokens to a 2d list of indices."""
        indices_2d = []
        for tokens in tokens_2d:
            indices = self.convert_tokens_to_indices(tokens)
            indices = indices[:max_seq_length]
            indices += [self("<pad>")] * (max_seq_length - len(indices))
            indices_2d.append(indices)
        return indices_2d

    def batch_decode_2d(self, indices_2d: list[list[int]]) -> list[list[str]]:
        """Converts a 2d list of indices to a 2d list of tokens."""
        tokens_2d = []
        for indices in indices_2d:
            tokens = self.convert_indices_to_tokens(indices)
            tokens_2d.append(tokens)
        return tokens_2d

