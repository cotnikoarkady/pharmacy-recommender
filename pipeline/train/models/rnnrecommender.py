from typing import Optional

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.utils.rnn as rnn_utils


class AttentionRNN(nn.Module):
    """
    Args:
        feature_size: the hidden feature size.
        dropout: dropout rate. Default is 0.5.
    """

    def __init__(
        self,
        feature_size: int,
        dropout: float = 0.5,
    ):
        super().__init__()
        self.feature_size = feature_size
        self.dropout = dropout
        self.dropout_layer = nn.Dropout(p=self.dropout)

        self.alpha_gru = nn.GRU(feature_size, feature_size, batch_first=True)
        self.beta_gru = nn.GRU(feature_size, feature_size, batch_first=True)

        self.alpha_li = nn.Linear(feature_size, 1)
        self.beta_li = nn.Linear(feature_size, feature_size)

    @staticmethod
    def reverse_x(input, lengths):
        """Reverses the input."""
        reversed_input = input.new(input.size())
        for i, length in enumerate(lengths):
            reversed_input[i, :length] = input[i, :length].flip(dims=[0])
        return reversed_input

    def compute_alpha(self, rx, lengths):
        """Computes alpha attention."""
        rx = rnn_utils.pack_padded_sequence(rx, lengths, batch_first=True, enforce_sorted=False)
        g, _ = self.alpha_gru(rx)
        g, _ = rnn_utils.pad_packed_sequence(g, batch_first=True)
        attn_alpha = torch.softmax(self.alpha_li(g), dim=1)
        return attn_alpha

    def compute_beta(self, rx, lengths):
        """Computes beta attention."""
        rx = rnn_utils.pack_padded_sequence(rx, lengths, batch_first=True, enforce_sorted=False)
        h, _ = self.beta_gru(rx)
        h, _ = rnn_utils.pad_packed_sequence(h, batch_first=True)
        attn_beta = torch.tanh(self.beta_li(h))
        return attn_beta

    def forward(
        self,
        x: torch.tensor,
        mask: Optional[torch.tensor] = None,
    ) -> tuple[torch.tensor, torch.tensor]:
        """Forward propagation.

        Args:
            x: a tensor of shape [batch size, sequence len, feature_size].
            mask: an optional tensor of shape [batch size, sequence len], where
                1 indicates valid and 0 indicates invalid.

        Returns:
            c: a tensor of shape [batch size, feature_size] representing the
                context vector.
        """
        # rnn will only apply dropout between layers
        x = self.dropout_layer(x)
        batch_size = x.size(0)
        if mask is None:
            lengths = torch.full(size=(batch_size,), fill_value=x.size(1), dtype=torch.int64)
        else:
            lengths = torch.sum(mask.int(), dim=-1).cpu()
        rx = self.reverse_x(x, lengths)
        attn_alpha = self.compute_alpha(rx, lengths)
        attn_beta = self.compute_beta(rx, lengths)
        c = attn_alpha * attn_beta * x  # (patient, sequence len, feature_size)
        c = torch.sum(c, dim=1)  # (patient, feature_size)
        return c


class RNNRecommender(nn.Module):
    """

    Args:
        voc_size: size of the feature vocabulary
        padding_idx: index for padding token
        output_size: size of the label vocabulary (number of drugs)
        embedding_dim: the embedding dimension. Default is 128.
    """

    def __init__(self, voc_size, padding_idx, output_size, embedding_dim: int = 128):
        super().__init__()
        self.embedding_dim = embedding_dim
        self.condition_embedding = nn.Embedding(voc_size, self.embedding_dim, padding_idx=padding_idx)
        self.attention_rnn = AttentionRNN(feature_size=embedding_dim)
        self.fc = nn.Linear(self.embedding_dim, output_size)

    def forward(self, x) -> dict[str, torch.Tensor]:
        """Forward propagation.

        Args:
            x: tokenized data

        Returns:
            y_prob: a tensor representing the predicted probabilities.
        """

        patient_emb = []
        # (patient, code, embedding_dim)
        x = self.condition_embedding(x)
        # (patient, code)
        mask = torch.sum(x, dim=2) != 0
        # (patient, hidden_dim)
        x = self.attention_rnn(x, mask)

        patient_emb.append(x)
        patient_emb = torch.cat(patient_emb, dim=1)
        # (patient, label_size)
        logits = self.fc(patient_emb)
        y_prob = torch.sigmoid(logits)
        return y_prob

    def calculate_loss(self, y_prob, y_true):
        return F.binary_cross_entropy(y_prob, y_true)
