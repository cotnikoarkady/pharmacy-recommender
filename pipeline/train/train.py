import logging
import os
import pickle
import random
import shutil

import luigi
import numpy as np
import torch
from torch.utils.data import DataLoader
import mlflow

from pipeline import config
from pipeline.config import DEFAULT_MODELS_DIR
from pipeline.features.samples import Samples
from pipeline.train.dataloader.sample_dataset import SampleDataset
from pipeline.train.dataloader.tokenizer import Tokenizer
from pipeline.train.utils.model_wrapper import DrugPredictionWrapper
from pipeline.train.utils.trainer import Trainer

log = logging.getLogger(__name__)


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
    os.environ["PYTHONHASHSEED"] = str(seed)


def calculate_class_weights(dataset: SampleDataset, drugs_tokenizer: Tokenizer):
    class_weights = torch.zeros(len(drugs_tokenizer))
    for sample in dataset:
        for drug in sample["drugs"]:
            drug_idx = drugs_tokenizer.token2idx[drug]
            class_weights[drug_idx] += 1
    class_weights = 1 - class_weights / class_weights.sum()
    return class_weights


def collate_fn_dict(batch):
    return {key: [d[key] for d in batch] for key in batch[0]}


class TrainedModel(luigi.Task):
    """
    This task trains a model on the sample dataset.
    """

    model_name: str = luigi.Parameter(default="rnnrecommender")
    epochs: int = luigi.Parameter(default=1)
    metrics: list[str] = luigi.Parameter(default=["pr_auc_samples", "f1_samples", "jaccard_samples", "accuracy", "precision_samples"])

    def requires(self):
        return Samples()

    def run(self):
        set_seed(42)
        with open(self.input().path, "rb") as f:
            samples = pickle.load(f)
        samples_dataset = SampleDataset(samples)
        drug_tokenizer = Tokenizer(samples_dataset.drugs_tokens)
        condition_tokenizer = Tokenizer(samples_dataset.conditions_tokens)
        model = get_model(self.model_name, drug_tokenizer, condition_tokenizer)
        wrapper = DrugPredictionWrapper(condition_tokenizer, model, drug_tokenizer)
        class_weights = calculate_class_weights(samples_dataset, drug_tokenizer)
        mlflow.set_experiment("train drug recommender")
        with mlflow.start_run():
            trainer = Trainer(
                model_wrapper=wrapper, metrics=self.metrics, class_weights=class_weights
            )
            train_ds, val_ds, test_ds = samples_dataset.split()
            train_data_loader = DataLoader(
                train_ds, batch_size=32, shuffle=True, num_workers=4, collate_fn=collate_fn_dict
            )
            val_data_loader = DataLoader(
                val_ds, batch_size=32, shuffle=False, num_workers=4, collate_fn=collate_fn_dict
            )
            test_data_loader = DataLoader(
                test_ds, batch_size=32, shuffle=False, num_workers=4, collate_fn=collate_fn_dict
            )
            trainer.train(train_data_loader, val_data_loader, test_data_loader, epochs=self.epochs)
            with self.output().temporary_path() as temp_output_path:
                torch.save(model, temp_output_path)
            eval_model(model, test_data_loader, self.epochs, 32)

    def output(self):
        return luigi.LocalTarget(os.path.join(DEFAULT_MODELS_DIR, f"{self.model_name}.pkl"))


def get_model(model_name, drug_tokenizer, condition_tokenizer):
    if model_name == "rnnrecommender":
        from pipeline.train.models.rnnrecommender import RNNRecommender
        voc_size = len(condition_tokenizer)
        padding_idx = condition_tokenizer("<pad>")
        output_size = len(drug_tokenizer)
        return RNNRecommender(voc_size, padding_idx, output_size)
    else:
        raise ValueError(f"Unknown model name: {model_name}")


def eval_model(trainer, test_loader, epochs_num, batch_size):
    mlflow.log_metrics(trainer.evaluate(test_loader))
    mlflow.pyfunc.log_model(
        artifact_path="model",
        python_model=trainer.model_wrapper,
        registered_model_name="drug_prediction_model",
    )
    mlflow.log_param("epochs_num", epochs_num)
    mlflow.log_param("batch_size", batch_size)
    mlflow.end_run()


def dump_best_model(experiment_name):
    best_run_df = mlflow.search_runs(
        order_by=[f"metrics.precision_samples"],
        max_results=1,
        experiment_names=[experiment_name],
    )
    if len(best_run_df.index) == 0:
        raise Exception(
            f"Found no runs for experiment '{experiment_name}'"
        )

    best_run = mlflow.get_run(best_run_df.at[0, "run_id"])
    best_model_uri = f"{best_run.info.artifact_uri}/model"
    best_model = mlflow.pyfunc.load_model(best_model_uri).unwrap_python_model()
    print(best_model)

    # print best run info
    log.info("Best run info:")
    log.info(f"Run id: {best_run.info.run_id}")
    log.info(f"Run parameters: {best_run.data.params}")
    log.info(
        "Run score: pr_auc_samples = {:.4f}".format(
            best_run.data.metrics["pr_auc_samples"]
        )
    )
    log.info(f"Run model URI: {best_model_uri}")
    model_path = config.BEST_MODELS_PATH
    shutil.rmtree(model_path, ignore_errors=True)
    mlflow.pyfunc.save_model(model_path, python_model=best_model, conda_env=None)
