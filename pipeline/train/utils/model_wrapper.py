import logging
from typing import Optional

import mlflow
import torch
import torch.nn as nn
import torch.nn.functional as F
from pipeline.train.dataloader.tokenizer import Tokenizer
log = logging.getLogger(__name__)


def batch_to_multi_hot(label: list[list[int]], num_labels: int) -> torch.tensor:
    """Converts label to multi hot format.

    Args:
        label: [batch size, *]
        num_labels: total number of labels

    Returns:
        multi_hot: [batch size, num_labels]
    """
    multi_hot = torch.zeros((len(label), num_labels))
    for i, l in enumerate(label):
        multi_hot[i, l] = 1
    return multi_hot


def select_top(number_to_select, prediction):
    top_drugs = torch.topk(prediction, number_to_select)[1]
    return top_drugs


class DrugPredictionWrapper(mlflow.pyfunc.PythonModel):
    def __init__(
            self,
            condition_tokenizer: Tokenizer,
            model: nn.Module,
            drug_tokenizer: Tokenizer,
            device: Optional[str] = None,
    ):
        super().__init__()
        self.model = model
        self.conditions_tokenizer: Tokenizer = condition_tokenizer
        self.drugs_tokenizer: Tokenizer = drug_tokenizer
        if device is None:
            device = "cuda" if torch.cuda.is_available() else "cpu"
        self.device = device
        log.info(f"Device: {self.device}")

    def predict(self, context, model_input, top_k=5):
        conditions_indices = self.tokenize_model_input([model_input])
        probabilities = self.model(conditions_indices)[0]
        top_drugs = select_top(top_k, probabilities)
        atc_codes = {
            self.drugs_tokenizer.idx2token[
                predicted_index.item()
            ]: probabilities[predicted_index.item()]
            for predicted_index in top_drugs
        }
        return atc_codes

    def forward(self, batch, class_weights=None):
        # (batch_size, sequence_length)
        x = batch["conditions"]
        true_drugs = batch["drugs"]
        x = self.tokenize_model_input(x)

        labels_index = self.drugs_tokenizer.batch_encode_2d(true_drugs, max_seq_length=512)
        num_labels = len(self.drugs_tokenizer)
        y_true = batch_to_multi_hot(labels_index, num_labels).to(self.device)
        y_prob = self.model(x)

        if class_weights is not None:
            loss = F.binary_cross_entropy(y_prob, y_true, weight=class_weights)
        else:
            loss = F.binary_cross_entropy(y_prob, y_true)
        return y_true, y_prob, loss

    def untokenize_model_output(self, model_output):
        return self.drugs_tokenizer.batch_decode_2d(model_output)

    def tokenize_model_input(self, model_input):
        return torch.tensor(self.conditions_tokenizer.batch_encode_2d(model_input, max_seq_length=512)).to(
            self.device
        )

    def load_model(self, path):
        self.model = torch.load(path).to(self.device)

    def save_model(self, path):
        torch.save(self.model, path)

    def to(self, device):
        self.model.to(device)
        self.device = device
        return self
