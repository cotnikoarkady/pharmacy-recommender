import logging
from typing import Dict, List, Optional, Type

import mlflow
import numpy as np
import torch
from pyhealth.metrics import multilabel_metrics_fn
from pipeline.train.utils.model_wrapper import DrugPredictionWrapper
from torch.optim import Optimizer
from torch.utils.data import DataLoader
from tqdm import tqdm
from tqdm.autonotebook import trange

log = logging.getLogger(__name__)


def is_best(best_score: float, score: float, monitor_criterion: str) -> bool:
    if monitor_criterion == "max":
        return score > best_score
    elif monitor_criterion == "min":
        return score < best_score
    else:
        raise ValueError(f"Monitor criterion {monitor_criterion} is not supported")


class Trainer:
    """Trainer for PyTorch models.

    Args:
        model_wrapper: mlflow.pyfunc.PythonModel instance
        metrics: List of metric names to be calculated. Default is None, which mean pr_auc_samples metric
    """

    def __init__(
        self,
        model_wrapper: DrugPredictionWrapper,
        metrics: Optional[List[str]] = None,
        class_weights: Optional[torch.Tensor] = None,
    ):
        self.model_wrapper = model_wrapper
        self.metrics = metrics
        self.class_weights = class_weights

        # logging
        log.info(self.model_wrapper.model)
        log.info(f"Metrics: {self.metrics}")

        log.info("")

    def train(
        self,
        train_dataloader: DataLoader,
        val_dataloader: Optional[DataLoader] = None,
        test_dataloader: Optional[DataLoader] = None,
        epochs: int = 5,
        optimizer_class: Type[Optimizer] = torch.optim.Adam,
        optimizer_params: Optional[Dict[str, object]] = None,
        weight_decay: float = 0.0,
        max_grad_norm: float = None,
        monitor: Optional[str] = None,
        monitor_criterion: str = "max",
    ):
        """Trains the model.

        Args:
            train_dataloader: Dataloader for training.
            val_dataloader: Dataloader for validation. Default is None.
            test_dataloader: Dataloader for testing. Default is None.
            epochs: Number of epochs. Default is 5.
            optimizer_class: Optimizer class. Default is torch.optim.Adam.
            optimizer_params: Parameters for the optimizer. Default is {"lr": 1e-3}.
            weight_decay: Weight decay. Default is 0.0.
            max_grad_norm: Maximum gradient norm. Default is None.
            monitor: Metric name to monitor. Default is None.
            monitor_criterion: Criterion to monitor. Default is "max".
        """
        if optimizer_params is None:
            optimizer_params = {"lr": 1e-3}

        # logging
        log.info("Training:")
        log.info(f"Batch size: {train_dataloader.batch_size}")
        log.info(f"Optimizer: {optimizer_class}")
        log.info(f"Optimizer params: {optimizer_params}")
        log.info(f"Weight decay: {weight_decay}")
        log.info(f"Max grad norm: {max_grad_norm}")
        log.info(f"Val dataloader: {val_dataloader}")
        log.info(f"Monitor: {monitor}")
        log.info(f"Monitor criterion: {monitor_criterion}")
        log.info(f"Epochs: {epochs}")

        # set optimizer
        param = list(self.model_wrapper.model.named_parameters())
        no_decay = ["bias", "LayerNorm.bias", "LayerNorm.weight"]
        optimizer_grouped_parameters = [
            {
                "params": [p for n, p in param if not any(nd in n for nd in no_decay)],
                "weight_decay": weight_decay,
            },
            {
                "params": [p for n, p in param if any(nd in n for nd in no_decay)],
                "weight_decay": 0.0,
            },
        ]
        optimizer = optimizer_class(optimizer_grouped_parameters, **optimizer_params)

        # initialize
        data_iterator = iter(train_dataloader)
        best_score = -1 * float("inf") if monitor_criterion == "max" else float("inf")
        steps_per_epoch = len(train_dataloader)
        global_step = 0

        # epoch training loop
        for epoch in range(epochs):
            training_loss = []
            self.model_wrapper.model.zero_grad()
            self.model_wrapper.model.train()
            # batch training loop
            log.info("")
            for _ in trange(
                steps_per_epoch,
                desc=f"Epoch {epoch} / {epochs}",
                smoothing=0.05,
            ):
                try:
                    data = next(data_iterator)
                except StopIteration:
                    data_iterator = iter(train_dataloader)
                    data = next(data_iterator)
                # forward
                y_true, y_prob, loss = self.model_wrapper.forward(
                    data, class_weights=self.class_weights
                )

                # backward
                loss.backward()
                if max_grad_norm is not None:
                    torch.nn.utils.clip_grad_norm_(
                        self.model_wrapper.model.parameters(), max_grad_norm
                    )
                # update
                optimizer.step()
                optimizer.zero_grad()
                training_loss.append(loss.item())
                global_step += 1
            # log and save
            log.info(f"--- Train epoch-{epoch}, step-{global_step} ---")
            log.info(f"loss: {sum(training_loss) / len(training_loss):.4f}")

            # validation
            if val_dataloader is not None:
                scores = self.evaluate(val_dataloader)
                log.info(f"--- Eval epoch-{epoch}, step-{global_step} ---")
                for key in scores.keys():
                    log.info("{}: {:.4f}".format(key, scores[key]))
                    mlflow.log_metrics(scores)
                # save best model
                if monitor is not None:
                    score = scores[monitor]
                    if is_best(best_score, score, monitor_criterion):
                        log.info(
                            f"New best {monitor} score ({score:.4f}) "
                            f"at epoch-{epoch}, step-{global_step}"
                        )
                        best_score = score
                        self.model_wrapper.save_model("best.pth")

        log.info("Loaded best model")
        self.model_wrapper.load_model("best.pth")

        # test
        if test_dataloader is not None:
            scores = self.evaluate(test_dataloader)
            log.info(f"--- Test ---")
            for key in scores.keys():
                log.info("{}: {:.4f}".format(key, scores[key]))

    def inference(self, dataloader):
        """Model inference.

        Args:
            dataloader: Dataloader for evaluation.

        Returns:
            y_true_all: List of true labels.
            y_prob_all: List of predicted probabilities.
            loss_mean: Mean loss over batches.
        """
        loss_all = []
        y_true_all = []
        y_prob_all = []
        for data in tqdm(dataloader, desc="Evaluation"):
            self.model_wrapper.model.eval()
            with torch.no_grad():
                y_true, y_prob, loss = self.model_wrapper.forward(data)
                loss_all.append(loss.item())
                y_true_all.append(y_true.cpu())
                y_prob_all.append(y_prob.cpu())
        loss_mean = sum(loss_all) / len(loss_all)
        y_true_all = np.concatenate(y_true_all, axis=0)
        y_prob_all = np.concatenate(y_prob_all, axis=0)
        return y_true_all, y_prob_all, loss_mean

    def evaluate(self, dataloader) -> Dict[str, float]:
        """Evaluates the model.

        Args:
            dataloader: Dataloader for evaluation.

        Returns:
            scores: a dictionary of scores.
        """
        y_true_all, y_prob_all, loss_mean = self.inference(dataloader)
        scores = multilabel_metrics_fn(y_true_all, y_prob_all, metrics=self.metrics)
        scores["loss"] = loss_mean
        return scores
